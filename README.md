# Project Proposal

Group Project Milestone #1

Due: 11/1/21

Each team will submit a brief project proposal, roughly one page in length, that identifies a business problem, a description and link to the data set for analysis, and a project plan. In addition, a discussion on the analysis that is being proposed and how it will answer the team's research problem should be provided. This milestone is worth 15/100 points.
